Title: Kontakt und Impressum
page_order: 6

Der Verein trägt den Namen *Fachschaft und Alumni der Informatik Göttingen e.V.*, kurz *InFA e.V.*
Es können __beide Namen__ verwendet werden.


**Kontakt**

Bitte kontaktieren Sie uns nach Möglichkeit direkt per E-Mail:


`infa {aett} uni-goettingen.de`

[`PGP-Key`](/files/pgpkey-infa.asc)


**Anschrift**
```
InFA e.V.
c/o AStA
Goßlerstr. 16a
37073 Göttingen
```

Registriert beim Registergericht Göttingen: `VR 202145`

StNr. `20/206/31711`
