Title: Aktuelles
page_order: 3

## Eigene und befreundete Veranstaltungen

- Dezember 2020: Online-Weihnachtstreffen InFA (Einladung per Mail)
- 06.01.2021 Cookie Talk (Keksseminar): "Was kommt nach dem Info-Studium?"
- 12.-16.05.2021: KIF 49,0 (in Göttingen)
- TBA: studentische Informatiktage 2021
- TBA: Mitgliederversammlung 2021
- TBA: Alumni-Treffen 2021
- TBA: KIF 49,5 (in Dortmund)
