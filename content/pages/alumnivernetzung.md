Title: Alumnivernetzung
page_order: 4

## Alumnivernetzung

Unser Ziel ist es in einem jährlichen Rhythmus Alumni-Meetups zu veranstalten und alle paar Jahre ein größeres Treffen. Kommendes Jahr wird das parallel zu den Studentischen Informatiktagen sein, die dieses Jahr vom 09.06.2023 bis zum 11.06.2023 stattfinden. Ein genauer Termin steht noch nicht fest, da auch überlegt wird an diesem Wochenende eine Absolvent:innenfeier zu veranstalten. Zu den Studentischen Informatiktagen kann man sich aber bereits anmelden und es werden auch noch Vorträge und Workshops gesucht: https://events.gwdg.de/e/git2023. Für Mitglieder und künftige Mitglieder weiterhin die Info, dass dort wahrscheinlich auch unsere Mitgliederversammlung stattfindet.

Um über News und weitere Alumnitreffen auf dem Laufenden gehalten zu werden, gibt es jetzt eine Mailingliste, auf der man sich einfach eintragen kann: https://listserv.gwdg.de/mailman/listinfo/informatik-alumnis. Wir bitten darum, dass sich nur Alumnis von Informatikstudiengängen der Universität Göttingen und Personen, die sich damit verbunden fühlen auf diese Mailingliste eintragen.

## Mitgliedschaft

Eine Mitgliedschaft im Verein ist grundsätzlich für natürliche und juristische Personen möglich. Sie kann [durch eine E-Mail an den Vorstand beantragt werden](mailto:infa@uni-goettingen.de?subject=Mitgliedschaftsantrag&body=Lieber%20Vorstand%2Cich%20w%C3%BCrde%20gerne%20dem%20Infa%20e.V.%20beitreten.%20Meine%20Daten%20sind%3Avollst%C3%A4ndiger%20Name%3A%20%5BTODO%5DE-Mail-Adresse%3A%20%5BTODO%5DStudium%3A%20%5Bz.B.%20Angewandte%20Informatik%20von%202010-2015%20an%20der%20Uni%20G%C3%B6ttingen%5DViele%20Gr%C3%BC%C3%9Fe!). Dazu reichen bereits wenige Zeilen:

```
Lieber Vorstand,

ich würde gerne dem Infa e.V. beitreten. Meine Daten sind:
vollständiger Name: [TODO]
E-Mail-Adresse: [TODO]
Studium: [z.B. Angewandte Informatik von 2010-2015 an der Uni Göttingen]

Viele Grüße!
```

Es wird ein Beitrag entsprechend der aktuell gültigen [Beitragsordnung](../files/beitragsordnung-2020-07-03.pdf) erhoben. Die Höhe des Beitrags beträgt **0€** jährlich (Stand Februar 2023) und kann von der Mitgliederversammlung neu bestimmt werden. Eine Befreiung von der Zahlung des Beitrags ist nach Beitragsordnung möglich (Stand November 2020).

Näheres regelt die [Satzung](/satzung/).
