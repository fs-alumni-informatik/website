Title: InFA e.V.
page_order: 2

*Fachschaft und Alumni der Informatik Göttingen e.V.*, kurz *InFA e.V.* (wie **In**formatik **F**achschaft **A**lumni), wurde am 17. Januar 2020 gegründet.

Der Verein setzt sich für die Studierendenschaft der Informatik und Data Science in Göttingen ein und fördert ihre Organe. Desweiteren unterstützt der Verein bei der Durchführung von Veranstaltungen und leistet Vernetzungsarbeit zwischen Studierenden, Lehrenden und Absolvent\*innen.

*InFA e.V.* ist registriert beim Registergericht Göttingen. Der Verein verfolgt gemeinnützige Zwecke und darf Zuwendungsbestätigungen ("Spendenbescheinigugen") ausstellen.

## Satzung

Die [Satzung](../files/satzung_2020-07-03.pdf) wurde zuletzt geändert am 03.07.2020 (Stand November 2020).
