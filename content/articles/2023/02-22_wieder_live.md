---
title: "Wieder da mit mehr Features"
date: 2023-02-22 19:00
---

Hi, wir sind wieder auf infagoe.de verfügbar. Nachdem wir zunächst infagoe.de 1 Jahr lang an schmutzige Domaingrabber verloren hatten, weil uns ein Fehler unterlaufen ist, als wir gleichzeitig den Registrar wechseln wollten und die Domain verlängern wollten, freuen wir uns jetzt besonders, dass wir die Domain nun wieder registrieren konnten und nicht mehr nur unter https://c101-201.cloud.gwdg.de/ erreichbar sind.

Im Zuge der Aktualisierung unserer Webpräsenz wollen wir nun auch endlich die große Masse von Alumnis zu vernetzen. Dazu gibt es zunächst zwei Möglichkeiten:

- parallel zu den Studentischen Informatiktagen, die dieses Jahr vom 09.06.2023 bis zum 11.06.2023 stattfinden, laden wir zu einem Alumnitreffen ein. Ein genauer Termin steht noch nicht fest, da auch überlegt wird an diesem Wochenende eine Absolvent:innenfeier zu veranstalten.
    - Zu den Studentischen Informatiktagen kann man sich aber bereits anmelden und es werden auch noch Vorträge und Workshops gesucht: https://events.gwdg.de/e/git2023.
    - für Mitglieder und künftige Mitglieder weiterhin die Info, dass dort wahrscheinlich auch unsere Mitgliederversammlung stattfindet
- um künftig über News und weitere Alumnitreffen auf dem Laufenden gehalten zu werden, gibt es jetzt eine Mailingliste, auf der man sich einfach eintragen kann: https://listserv.gwdg.de/mailman/listinfo/informatik-alumnis
