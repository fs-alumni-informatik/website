---
title: "Neue Website des InFA e.V."
date: 2020-11-17 12:00
---

Liebe Mitglieder,

nachdem der Verein inzwischen eingetragen und als (vorläufig) gemeinnützig anerkannt worden ist, haben wir nun endlich auch eine Website.
Wenn ihr Feedback geben möchtet, meldet euch gerne über die gewohnten Kanäle, oder im Zweifel über unsere Mailadresse.

Euer Vorstand
