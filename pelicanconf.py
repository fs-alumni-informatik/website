#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'InFA'
SITENAME = 'InFA e.V.'
SITEURL = ''

PATH = 'content'
PAGE_PATHS = ['pages']
ARTICLE_PATHS = ['articles']
STATIC_PATHS = ['files']

TIMEZONE = 'Europe/Paris'
DEFAULT_DATE_FORMAT = '%d.%m.%Y'

THEME = 'theme/eule'
DEFAULT_LANG = 'de'

MENUITEMS = [('Home', '/')]
PAGE_ORDER_BY = 'page_order'


# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_CATEGORY = "Allgemein"
DEFAULT_PAGINATION = 10
USE_FOLDER_AS_CATEGORY = False
DISPLAY_CATEGORIES_ON_MENU = False

ARTICLE_URL = 'articles/{date:%Y}/{slug}.html'
ARTICLE_SAVE_AS = 'articles/{date:%Y}/{slug}.html'
PAGE_URL = '{slug}/'
PAGE_SAVE_AS = '{slug}/index.html'

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
