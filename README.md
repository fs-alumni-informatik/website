# Infa e.V. Website
## deps
You need `pelican`.

## local dev
Install pelican and then run `pelican content/` from the project directory. The output will be in `output/`.

## Setup-Guide
If you need to set up a new sever -- for whatever reason -- you can liberally follow this instruction:

```bash
# Version 2021-09-12

# set more secure default password?
passwd
# set unattended upgrade settings
sudo vim /etc/apt/apt.conf.d/50unattended-upgrades

# setup nginx and letsencrypt
sudo apt install nginx
sudo snap install core; sudo snap refresh core
sudo snap install --classic certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot
sudo certbot --nginx
sudo certbot renew --dry-run
sudo systemctl enable --now nginx

# setup gitlab-runner
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt install gitlab-runner
# generate a new ssh key
sudo ssh-keygen
# allow login with this key
cat .ssh/id_gitlab-runner.pub >> .ssh/authorized_keys
# register runner with gitlab.gwdg.de (instructions can be found in the ci/cd settings inside the project)
sudo gitlab-runner register
# bash logout clears the console and gitlab runner doesn't like that
mv .bash_logout .bash_logout.old
sudo systemctl enable --now gitlab-runner

# setup pelican as a project dependency
sudo apt install pelican

# give the user rights for the web servers directory. the ci-pipeline must be able to copy files to this location
sudo chown -R cloud:cloud /var/www/html
sudo rm -rf /var/www/html/*

# finally reboot and check whether everything is started as expected
sudo reboot

# disable password login; IMPORTANT: add your keys first!
sudo vim /etc/ssh/sshd_config
```
